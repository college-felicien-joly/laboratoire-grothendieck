[Rendu du site](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/)

[Tutoriel 1 : structure du site](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/12_credits/credits/)

[Tutoriel 2 : changement des logos, couleurs](https://squidfunk.github.io/mkdocs-material/setup/)


Pour souligner du texte : \<ins>Texte\</ins>
