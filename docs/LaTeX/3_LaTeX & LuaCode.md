---
author: Anthony Salze
title: LaTeX & LuaCode
hide:
    -toc
    -integrate
    -footer
---

## <span style=color:Tomato> Introduction : exemple simple
### <span style=color:MediumSeaGreen> Présentation
Pour que certaines méthodes soient bien assimilées par nos élèves, il est parfois nécessaire de répéter les exercices. Avec **LuaLaTeX**, il est possible d'en générer rapidement avec des valeurs aléatoires et la correction associée.

Pour cela, il faudra utiliser les packages suivants :  
    - [luacode](https://ctan.org/pkg/luacode) de Manuel Pégourié-Gonnard ;  
    - [ProfCollege](https://www.ctan.org/pkg/profcollege) de Christophe Poulain.
  

### <span style=color:MediumSeaGreen> Préambule

??? abstract "Préambule"
    ```latex
    \documentclass[a4paper, 12pt]{report}
    \usepackage{lmodern}
    \usepackage[french]{babel}
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}
    \usepackage{geometry}
    \usepackage{luacode}
    \usepackage{ProfCollege}
    \usepackage{unicode-math}
    \newfontfamily\myfontScratch[]{FreeSans}
    \usepackage[explicit]{titlesec}
    \usepackage{hyperref}
    \geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}
    \renewcommand\thesection{Exercice \arabic{section}}
    ```

### <span style=color:MediumSeaGreen> Énoncé

??? abstract "Code tex : énoncé"
    ```latex
    \section{} \vspace{-0.5cm}

    %Génère les données aléatoires du problème.
    \begin{luacode}
	    a=math.random(3,9)
	    b=math.random(8,20)
	    c=math.random(19,30)
	    d=math.random(8,12)
    \end{luacode}

    %Énoncé de l'exercice
    Pour chaque figure, calculer la longueur manquante. Donner une valeur exacte, puis une valeur approchée au centième près.\\

    %Création de deux figures avec le package ProfCollege et les données aléatoires.
    \Pythagore[FigureSeule]{ILC}{\directlua{tex.print(a)}}{\directlua{tex.print(b)}}{} \hspace{5cm}
    \Pythagore[FigureSeule, Angle=30]{RTS}{\directlua{tex.print(c)}}{\directlua{tex.print(d)}}{}
    ```
Les commandes `\begin{luacode} \end{luacode}` permettent de créer du code **Lua** au sein de LaTeX.  
Ici, nous souhaitons générer 4 nombres aléatoires entiers. Pour cela, il faut utiliser l'instruction `math.random(borneinf,bornesup)`. Pour la variable `a`, nous obtiendrons un nombre compris entre $3$ et $9$.  
  
Pour afficher la valeur d'une variable dans le fichier PDF après compilation, il faut utiliser la commande suivante `\directlua{tex.print(nom_variable)}`.  
  
??? info "Résultat après compilation"
    ![Rendu Exercice](a_telecharger/LuaCode/LuaCode_001.png){ width=80%  : .center }


Avoir l'énoncé c'est bien beau, mais c'est mieux si la correction est générée en même temps.

### <span style=color:MediumSeaGreen> Correction
Sur cet exemple, il est possible d'obtenir rapidement la correction avec la commande `\Pythagore{}{}{}` du package [ProfCollege](https://www.ctan.org/pkg/profcollege).  

??? abstract "Code tex : correction"
    ```latex
        \renewcommand\thesubsection{Solution Exercice \arabic{subsection}}
        \subsection{} \vspace{-0.3cm}
        \begin{minipage}{0.45\textwidth}
            \Pythagore{ILC}{\directlua{tex.print(a)}}{\directlua{tex.print(b)}}{}
        \end{minipage}
        \hspace{0.05cm} \vrule \hspace{0.05cm}
        \begin{minipage}{0.45\textwidth}
            \Pythagore{RTS}{\directlua{tex.print(c)}}{\directlua{tex.print(d)}}{}
        \end{minipage}
    ```

??? info "Résultat après compilation"
    ![Rendu Correction](a_telecharger/LuaCode/LuaCode_002.png){ width=80%  : .center }


## <span style=color:Tomato> Syntaxe de base
Quelques instructions mathématiques utiles au collège en syntaxe **Lua**.  

$$
\begin{array}{|c|c|}\hline
    \textbf{**Instructions courantes**} & \textbf{**Description**} \\\hline
    \text{math.random(borneinf,bornesup)} & \text{Renvoie un nombre entier aléatoire compris entre borneinf et bornesup.}\\\hline
    \text{math.ceil($x$)} & \text{Renvoie l'arrondi par excès du nombre x.} \\\hline
    \text{math.floor($x$)} & \text{Renvoie l'arrondi par défaut du nombre x.} \\\hline
    \text{math.fmod($x$,$y$)} & \text{Renvoie le reste de la division euclidienne de x par y.} \\\hline
    \text{math.pi} & \text{Renvoie la valeur de $\pi$.} \\\hline
    \text{math.max($x_1$,...,$x_n$)} & \text{Renvoie le maximum de la liste de nombre $x_1,...,x_n$.} \\\hline
    \text{math.min($x_1$,...,$x_n$)} & \text{Renvoie le minimum de la liste de nombre $x_1,...,x_n$.} \\\hline
    \text{math.sqrt($x$)} & \text{Renvoie la valeur de la racine carrée du nombre $x$.} \\\hline
    \text{math.abs($x$)} & \text{Renvoie la valeur absolue du nombre $x$.} \\\hline
    &\\\hline
    \textbf{**Fonctions trigonométriques**} & \textbf{**Description**} \\\hline
    \text{math.cos($x$)} & \text{Renvoie le cosinus de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.sin($x$)} & \text{Renvoie le sinus de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.tan($x$)} & \text{Renvoie la tangente de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.tan($x$)} & \text{Renvoie la tangente de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.tan($x$)} & \text{Renvoie la tangente de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.acos($x$)} & \text{Renvoie l'arccosinus de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.asin($x$)} & \text{Renvoie l'arcsinus de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.atan($x$)} & \text{Renvoie l'arctangente de l'angle $x$ (doit être en radians).} \\\hline
    \text{math.deg($x$)} & \text{Renvoie la mesure en degrés de l'angle $x$ donnée en radians.} \\\hline
    \text{math.rad($x$)} & \text{Renvoie la mesure en radians de l'angle $x$ donnée en degrés.} \\\hline
\end{array}
$$

Les commandes `\begin{luacode} \end{luacode}` permettent de créer du code **Lua** au sein de LaTeX.  
  
Pour afficher la valeur d'une variable ou d'un calcul dans le PDF après compilation, il faut utiliser la commande suivante `\directlua{tex.print(nom_variable)}`.  


## <span style=color:Tomato> Créer un tableau de valeurs
En **Lua**, pour créer un tableau de valeurs, il suffit de lui donner un nom puis de lister les valeurs entre accolades séparés par une virgule.  

```latex
\begin{luacode}
    tableau = {5,8,7,20,12,9,28,11,16,33,48,13,36,39,65}
\end{luacode}
```

Si on veut accéder à une valeur particulière du tableau, il faut utiliser l'instruction suivante `nom_tableau[numéro_colonne_valeur]`.  
Dans notre exemple, j'aimerai accéder à la $7^{\text{e}}$ valeur de mon tableau. Pour cela, il faut taper le code suivant dans mon fichier .tex :  
```latex
La $7\ieme$ valeur de mon tableau est \directlua{tex.print(tableau[7])}.
```
Après compilation, le fichier PDF affiche : << La $7^{\text{e}}$ valeur de mon tableau est $28$. >>


## <span style=color:Tomato> Réaliser des calculs
Il est possible de réaliser des calculs en utilisant les instructions **Lua**.  
Par exemple, on souhaite calculer $A=\left[18,5 \times 2 - (30,1 + 3,9)\right] : 3$.  

!!! abstract "Code LaTeX"
    ```latex
    Calculer $A=\left[18,5 \times 2 - (30,1 + 3,9)\right] : 3$ en détaillant les différentes étapes.
    \begin{flalign*}
        A&=\left[18,5 \times 2 - (30,1 + 3,9)\right] : 3\\
        A&=\left[18,5 \times 2 - \directlua{tex.print(30.1 + 3.9)}\right] : 3\\
        A&=\left[\directlua{tex.print(18.5 *2)} - \directlua{tex.print(30.1 + 3.9)}\right] : 3\\
        A&=\directlua{tex.print(18.5 *2 - (30.1+3.9))} : 3\\
        A&=\directlua{tex.print((18.5 *2 - (30.1+3.9))/3)}
    \end{flalign*}
    ```

Voici le fichier obtenu après compilation :  
!!! danger "Après compilation"
    ![Rendu Correction](a_telecharger/LuaCode/LuaCode_003.png){ width=70%  : .center }

**Remarques**  
    1. Le symbole de la multiplication est \*  
    2. Celui de la division est /  
    3. :warning: Les virgules sont remplacées par des points  
    4. :warning: Même si, après calculs, le nombre obtenu est un nombre entier, le code **Lua** affichera tout de même un $0$ après la virgule.

**Solutions d'améliorations**  
    * _Remarque 3_ : Pour afficher une virgule à la place du point dans le résultat, il faut envelopper l'instruction `\directlua{}` par `\num{}`.  
    * _Remarque 4_ : Pour cet exemple, la commande **Lua** `math.floor` suffit pour afficher correctement le résultat désiré. Nous verrons dans une autre partie qu'il faut parfois un peu plus de travail pour obtenir l'arrondi désiré.

!!! abstract "Code LaTeX amélioré"
    ```latex
    Calculer $A=\left[18,5 \times 2 - (30,1 + 3,9)\right] : 3$ en détaillant les différentes étapes.
    \begin{flalign*}
        A&=\left[18,5 \times 2 - (30,1 + 3,9)\right] : 3\\
        A&=\left[18,5 \times 2 - \num{\directlua{tex.print(math.floor(30.1 + 3.9))}}\right] : 3\\
        A&=\left[\num{\directlua{tex.print(math.floor(18.5 *2))}} - \num{\directlua{tex.print(math.floor(30.1 + 3.9))}}\right] : 3\\
        A&=\num{\directlua{tex.print(math.floor(18.5 *2 - (30.1+3.9)))}} : 3\\
        A&=\num{\directlua{tex.print(math.floor((18.5 *2 - (30.1+3.9))/3))}}
    \end{flalign*}
    ```

!!! info "Après compilation - résultat amélioré"
    ![Rendu Correction](a_telecharger/LuaCode/LuaCode_004.png){ width=70%  : .center }

## <span style=color:Tomato> Utiliser des boucles if...then...else
```mermaid
    %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
    flowchart TB
        n0(if) --> n1(then)
        n0 --> n2(else)
```

## <span style=color:Tomato> Arrondir un nombre


