---
author: Anthony Salze
title: Cartes auto-correctives
hide:
    -toc
    -integrate
    -footer
---
Au cours de l'année scolaire 2023/2024, au sein de quelques classes de sixièmes et de quatrièmes, les élèves ont créé des questions/réponses sur les différents chapitres vus au cours de l'année.  
Par la suite, ce travail a été retranscrit en cartes auto-correctives via LaTeX grâce au package [ProfCollege](https://www.ctan.org/pkg/profcollege) de Christophe Poulain. Ci-dessous, se trouve les différents rendus ainsi que les fichiers LaTeX.

 
??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline:"
    === "Cartes format PDF"
        - [Thème : Calculs](a_telecharger/Cartes/6_Cartes_Calculs.pdf)
        - [Thème : Nombres](a_telecharger/Cartes/6_Cartes_Nombres.pdf)
        - [Thème : Géométrie bases](a_telecharger/Cartes/6_Cartes_Géométrie_bases.pdf)
        - [Thème : Polygones](a_telecharger/Cartes/6_Cartes_Polygones.pdf)
        - [Thème : Solides](a_telecharger/Cartes/6_Cartes_Solides.pdf)
        - [Thème : Aires, périmètres](a_telecharger/Cartes/6_Cartes_Aires_périmètres.pdf)
    === "Cartes format LaTeX"
        - [Thème : Calculs](a_telecharger/Cartes/6_Cartes_Calculs.tex)
        - [Thème : Nombres](a_telecharger/Cartes/6_Cartes_Nombres.tex)
        - [Thème : Géométrie bases](a_telecharger/Cartes/6_Cartes_Géométrie_bases.tex)
        - [Thème : Polygones](a_telecharger/Cartes/6_Cartes_Polygones.tex)
        - [Thème : Solides](a_telecharger/Cartes/6_Cartes_Solides.tex)
        - [Thème : Aires, périmètres](a_telecharger/Cartes/6_Cartes_Aires_périmètres.tex)
        
<span style=color:CornflowerBlue>_Dernière mise à jour : le 26/09/24_