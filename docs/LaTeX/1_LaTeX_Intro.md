---
author: Anthony Salze & Jean-Sébastien Sip
title: Accueil
hide:
    -toc
    -integrate
    -footer
---
LaTeX pour de belles présentations et activités.  
  
$\displaystyle <P,Q> = \int_0^1 P(t)Q(t)dt$  
  
$\displaystyle \forall (P,Q) \in (\mathbb{K}[X])^2, \forall k \in \mathbb{N},(PQ)^{(k)} = \sum_{i=0}^{k} \binom{k}{i} P^{(i)}Q^{(k-i)}$