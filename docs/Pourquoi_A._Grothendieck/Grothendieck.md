---
author: Anthony Salze & Jean-Sébastien Sip & Jérôme Scrufari
title: Alexandre Grothendieck
hide:
    -navigation
    -toc
    -integrate
    -footer
---
« _Une vision nouvelle est une chose si vaste, que son apparition ne peut sans doute se situer à un moment particulier, mais qu’elle doit pénétrer et prendre possession progressivement pendant de longues années, si ce n’est sur des générations, de celui ou de ceux qui scrutent et qui contemplent ; comme si des yeux nouveaux devaient laborieusement se former, derrière les yeux familiers auxquels ils sont appelés à se substituer peu à peu_. »

<div style="text-align:right">A. Grothendieck, <strong>Récoltes et Semailles</strong>, p. P24 Collection Tel, Gallimard (2022)</div>

&nbsp;

Alexandre Grothendieck (1928 – 2014) est sans aucun doute l'un des plus grands mathématiciens de la seconde moitié du XXème siècle. Ces travaux ont véritablement révolutionné le monde des Mathématiques, notamment en Géométrie Algébrique avec la découverte, entre autres, de la notion fondamentale de Topos.

Personnage particulièrement atypique de la scène mathématique internationale, il a décidé de s’en éloigner définitivement dans les années 1970, refusant même la Médaille Fields qui lui a été décernée en 1966. Mais il aura poursuivi son œuvre visionnaire, en Mathématique notamment, laissant à sa mort un héritage inestimable de plusieurs dizaines de milliers de pages. 

![](images/AGro2.jpg){ width=50%; : .center }
