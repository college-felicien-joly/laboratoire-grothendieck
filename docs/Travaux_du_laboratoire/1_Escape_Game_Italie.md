---
author: Anthony Salze & Jean-Sébastien Sip
title: Escape Game en Italie
hide:
    -toc
    -integrate
    -footer
---

Deux professeurs du collège, Mme BUCHWALD (Professeur d'Italien) et M. SIP (Professeur de Mathématiques) se sont engagés dans la réalisation d'un Escape Game scénarisé en trois parties (saisons).
Chacune d'entre elles est destinée à un niveau particulier (la saison 1 pour les classes de cinquièmes, la saison 2 pour les classes de quatrièmes et enfin la saison 3 pour les classes de troisièmes).
L'ensemble forme un scénario complet s'étalant ainsi sur la totalité du cycle 4.

Dans chacune de ces trois saisons, les événements ont lieu dans une ville particulière de l'Italie . Les élèves se retrouvent ainsi plongés au cœur même de la culture italienne à travers un conflit séculaire opposant les plus grandes familles régnantes d'Italie. 

Les élèves seront amenés à résoudre de nombreuses énigmes mathématiques et logiques tout en manipulant plusieurs documents rédigés en italien. Cette longue enquête leur demandera de mobiliser l'ensemble des compétences acquises au collège, tant en cours de Mathématiques qu'en cours d'Italien.

Les élèves vont alors incarner plusieurs investigateurs :

* la Polizia de Florence
* une cellule universitaire de la bibliothèque centrale de Florence
* des journalistes de La Nazione
* un groupe de détectives mandatés par le Vatican

## Saison 1 : Meurtre à Florence

![](images/Collage_Firenze1.jpg){ width=50%; : .center }

### Synopsis

Depuis près de huit siècles les maisons Médicis, Pazzi et Pucci mènent à Florence une guerre secrète cherchant à obtenir le monopole de la gestion de toutes les banques d'Italie. En particulier, chacune nourrit l'espoir d'être le banquier officiel de sa Sainteté au Vatican. L'Histoire n'a retenu que la conjuration des Pazzi contre les Médicis mais en réalité, la famille Pucci attisait ce conflit en vue de devenir la principale famille régnante à Florence.

Dimanche 22 Août 2021. Florence se réveille dans l'horreur. Le prêtre préposé à l'office de ce dimanche au Duomo fit une macabre découverte. Le corps d'un haut banquier de Florence gisait dans une mare de sang près de l'autel. Il s'agit de Mario Venturi, administrateur depuis 2017 de la Banque d'Italie situé au 37/39 de la Via dell'Oruiolo et descendant de Jean de Médicis, banquier et fondateur de la maison des Médicis. Il avait été contacté trois jours plus tôt par son ami Sidney Chalhoub, spécialiste de la période Médicis en Italie à l'université d'Harvard.

### Galerie

![](images/Photo_01_escape1.jpg){ width=50%; : .center }

![](images/Photo_02_escape1.jpg){ width=50%; : .center }

![](images/Photo_03_escape1.jpg){ width=50%; : .center }

![](images/Photo_04_escape1.jpg){ width=50%; : .center }

![](images/Photo_05_escape1.jpg){ width=50%; : .center }

![](images/Photo_06_escape1.jpg){ width=50%; : .center }

## Saison 2 : Poursuite à Rome

![](images/Rome_image.jpg){ width=50%; : .center }

### Synopsis

Rome vit dès le X$^{\text{ième}}$ siècle l’émergence d'une nouvelle famille : les Frangipani. Il s'agit d'une famille dont les racines remontent à la Rome antique à travers une prétendue descendance de la famille Anicienne. Cette famille fortifia le Colisée le transformant en une véritable forteresse au XII$^{\text{ième}}$ siècle. En réalité, les Frangipani s'adonnaient à l'occulte et à l'alchimie. Ils créèrent un ordre religieux qu'ils établirent au Colisée afin d'y réaliser de nombreuses expériences. 

Vous reprenez connaissance avec une forte douleur à l'arrière du crâne et vous constatez avec stupeur que vous êtes enfermés dans un cachot humide et glacial. Celui-ci est inondé par une faible lueur qui vous parvient d'un couloir dont le fond se perd dans l'obscurité. C'est dans un silence inquiétant que s'écoulent de longues heures qui vous semblent être une éternité. Puis, soudain, un homme de haute stature frappe aux barreaux de votre cellule à l'aide d'un objet que vous identifiez à une matraque. Vous sortez définitivement de votre somnolence moribonde au moment où l'homme prend la parole : « Tenez, voilà pour vous ! ». Une maigre pitance constituée d'une miche de pain et d'une cruche vous est lancée négligemment au milieu de la cellule. Péniblement vous saisissez le pain. Instinctivement vous observez les lieux. L'homme s'est assis près de la cellule pour commencer son repas, loin de la frugalité du vôtre… 

### Galerie

![](images/Photo_01_escape2.jpg){ width=50%; : .center }

![](images/Photo_02_escape2.jpg){ width=50%; : .center }

![](images/Photo_03_escape2.jpg){ width=50%; : .center }

![](images/Photo_04_escape2.jpg){ width=50%; : .center }

![](images/Photo_05_escape2.jpg){ width=50%; : .center }

![](images/Photo_06_escape2.jpg){ width=50%; : .center }

![](images/Photo_07_escape2.jpg){ width=50%; : .center }

![](images/Photo_08_escape2.jpg){ width=50%; : .center }

![](images/Photo_09_escape2.jpg){ width=50%; : .center }

![](images/Photo_10_escape2.jpg){ width=50%; : .center }

![](images/Photo_11_escape2.jpg){ width=50%; : .center }

![](images/Photo_12_escape2.jpg){ width=50%; : .center }

![](images/Photo_13_escape2.jpg){ width=50%; : .center }

![](images/Photo_14_escape2.jpg){ width=50%; : .center }

![](images/Photo_15_escape2.jpg){ width=50%; : .center }

## Saison 3 : A venir...