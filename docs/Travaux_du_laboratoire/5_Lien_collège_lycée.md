---
author: Anthony Salze & Jean-Sébastien Sip
title: Collège - Lycée
hide:
    -toc
    -integrate
    -footer
---
Lien entre le collège et le lycée.