---
author: Anthony Salze & Jean-Sébastien Sip
title: Cordées
hide:
    -toc
    -integrate
    -footer
---
# Cordées de la Réussite

Le dispositif "cordées de la réussite" constitue un axe essentiel d'égalité des chances. 
Les cordées de la réussite font partie intégrante du programme pluriannuel d'orientation (PPO) mis en place dans les établissements du secondaire, autour de trois objectifs :

* Élaborer son projet d'orientation 
* Découvrir le monde économique, professionnel et les métiers
* Découvrir les différentes voies de formation. 

Les cordées de la réussite suscitent l'ambition scolaire des élèves par un projet éducatif et pédagogique qui lève les obstacles psychologiques, sociaux, géographiques et culturels susceptibles de freiner l'accès des jeunes aux formations de l'enseignement supérieur et particulièrement aux filières d'excellence. 

L'accompagnement de la 3e jusqu'au baccalauréat et à l'enseignement supérieur favorise des parcours d'orientation inscrits dans un continuum qui facilite l'adaptation lors des changements d'établissement et l'articulation entre les différents cycles d'apprentissage. 

Afin de faire découvrir l'ensemble des formations aux jeunes, les cordées de la réussite contribuent à développer la connaissance du monde économique et professionnel, portée notamment par les parcours en :

* Classes préparatoires aux grandes écoles 
* B.U.T.
* Écoles de la fonction publique 

![](images/Cordées/cord_01.jpg){ width=50%; : .center }

![](images/Cordées/cord_02.jpg){ width=50%; : .center }

![](images/Cordées/cord_03.jpg){ width=50%; : .center }

![](images/Cordées/cord_04.jpg){ width=50%; : .center }

![](images/Cordées/cord_05.jpg){ width=50%; : .center }

![](images/Cordées/cord_06.jpg){ width=50%; : .center }

![](images/Cordées/cord_07.jpg){ width=50%; : .center }

![](images/Cordées/cord_08.jpg){ width=50%; : .center }

![](images/Cordées/cord_09.jpg){ width=50%; : .center }

![](images/Cordées/cord_10.jpg){ width=50%; : .center }

![](images/Cordées/cord_11.jpg){ width=50%; : .center }
