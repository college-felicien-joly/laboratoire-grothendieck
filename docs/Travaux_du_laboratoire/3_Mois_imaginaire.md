---
author: Anthony Salze & Jean-Sébastien Sip
title: Mois de l'imaginaire
hide:
    -toc
    -integrate
    -footer
---

# Le mois de l'imaginaire !

Le mois de l'imaginaire est un projet qui se décline en de nombreuses activités :

* une activité auprès des classes de sixièmes dans l'univers de Harry Potter,
* une séance de cinéma concernant les classes de quatrièmes avec la projection du film Sleepy Hollow, réalisé par Tim Burton. Cette séance s'accompagne d'une activité faite en classe sur les Sorcières de Salem,
* une séance de type jeu de rôles réalisée au CDI auprès d'élèves volontaires,
* une séance de fabrication de potions en cours de Sciences Physiques.

## La rentrée à Poudlard

C'est la rentrée ! L'agitation habituelle est déjà palpable dans les rangs alors que les élèves s'alignent deux par deux le long du CDI, s'interrogeant déjà sur le choix de leur maison... Une fois installés, les voici propulsés dans la salle commune de Poudlard devant leurs professeurs, pour certains s'étant affublés du costume de rigueur !

Les élèves vont déterminer par des jets de dés leurs propres caractéristiques physiques et magiques comme s'ils réalisaient leur rentrée à Poudlard. Ils se verront également attribuer une baguette magique en lien avec les caractéristiques obtenues précédemment ainsi que leur maison d'appartenance.
Enfin, ils pourront prendre connaissance des compétences magiques dans lesquelles ils sont susceptibles d'exceller.

![](images/mois_imaginaire_01.png){ width=50%; : .center }

## Sleepy Hollow et les Sorcières de Salem

Dans ce film où se mêlent démarche scientifique et fantastique, les élèves pourront retenir une phrase : "les apparences sont trompeuses". Lors du cours sur les triangles isométriques en mathématiques, nos élèves pourront mettre en œuvre la recherche d'indices, de (propriétés) témoins afin de mener une démonstration sur le même mode qu'une enquête policière. Une scène particulière du film sera en effet analysée à cette fin.

Enfin, en vue d'aiguiser l'esprit critique de nos élèves, une séance de mise en œuvre de la démarche scientifique sera proposée autour du cas des Sorcières de Salem. L'activité consiste à trouver une solution rationnelle expliquant les phénomènes "paranormaux" qui ont secoués cette ville de la côte est des États-Unis à la fin du XVII$^{\text{ième}}$ siècle.

![](images/mois_imaginaire_03.png){ width=50%; : .center }

## Le Manoir de l'Enfer

Vous avez fait la plus grande erreur de votre vie en vous réfugiant dans le Manoir de l’Enfer. La terrible tempête qui fait rage au-dehors ne représente qu’un bien faible danger, comparée aux terrifiantes aventures qui vous attendent à l’intérieur de cette demeure maléfique.

Qui peut dire combien de voyageurs malchanceux, cherchant comme vous un abri, ont péri dans les murs du Manoir du Comte de Brume. Sachez que la nuit qui commence s’inscrira pour toujours parmi les plus effroyables souvenirs de votre existence... si toutefois vous vivez assez longtemps pour en garder mémoire. 

![](images/mois_imaginaire_02b.png){ width=50%; : .center }

## Potions explosives !

En cours de Physique-Chimie, les élèves courageux de sixième pourront réaliser des potions particulièrement insolites permettant de les initier à la notion de transformation chimique. Ils découvriront notamment la chromatographie, la notion de densité ainsi que le caractère missible de certain liquide.

![](images/mois_imaginaire_02.png){ width=50%; : .center }

## Exposition

![](images/mois_imaginaire_03b.png){ width=50%; : .center }

![](images/mois_imaginaire_04.png){ width=50%; : .center }