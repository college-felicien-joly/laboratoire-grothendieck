---
author: Anthony Salze & Jean-Sébastien Sip
title: EPI
hide:
    -toc
    -integrate
    -footer
---

# EPI au collège

## Un génie à Félicien Joly !

Une exposition consacrée à Léonard de Vinci, conçue et réalisée par les élèves italianistes de troisième et leurs professeurs d’italien et de mathématiques, a investi le hall du collège.

Celle-ci retrace la vie, les œuvres majeures, les inventions, et quelques curiosités sur le polymathe florentin. Des maquettes et les représentations des élèves d’une figure géométrique figurant dans le célèbre Codex Atlanticus complètent l’exposition.

Lors de ce travail interdisciplinaire, les élèves ont pu découvrir les multiples facettes de l’homme (peintre, architecte, ingénieur, scientifique…) ainsi que le vocabulaire en italien de certaines notions mathématiques (notamment en géométrie). Impliqués dans les apprentissages, ils ont fait preuve d’une grande curiosité et d’un entrain remarquable à l’égard de cette figure emblématique de la Renaissance. Ainsi quelques élèves volontaires se sont proposés de présenter l’exposition aux élèves de cinquième, les accompagnant dans les activités ludo-pédagogiques (quiz, écriture spéculaire, jeux de différences, mots mêlés…) préparées par les enseignants.

Découvrez les photos de l’exposition sans plus tarder !

![](images/EPI_Leonard_photo_01.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_02.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_03.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_04.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_05.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_06.jpg){ width=50%; : .center }

![](images/EPI_Leonard_photo_07.jpg){ width=50%; : .center }

## Le Nombre d'Or

Cet EPI a permis aux élèves de se plonger dans la douce atmosphère de la Renaissance italienne à travers la découverte de quelques noms des plus illustres parmi les sculpteurs, architectes, peintres et bien entendu, les mathématiciens. Ils ont pu entrevoir la richesse que cette période faste de l'histoire de l'Italie a su apporter dans le développement culturel de l'Humanité et ce, jusqu'à de nos jours. Les élèves ont alors réstitué le fruit de leurs découvertes dans une exposition qui s'est tenue dans le hall du collège.

![](images/EPI_nbre_dor_01.jpg){ width=50%; : .center }

![](images/EPI_nbre_dor_02.jpg){ width=50%; : .center }

Nous nous sommes penchés avec une attention toute particulière sur un nombre des plus étrange et mystérieux : le Nombre d'Or dont l'ubiquité dans la Nature ne cesse aujourd'hui de faire couler beaucoup d'encre. Les élèves ont pu découvrir l'émergence de ce nombre au sein de notre univers ainsi que dans de nombreuses productions artistiques (certaines issues de la Renaissance). Enfin, ils se sont familiarisés avec cette notion à travers la célèbre suite de Fibonacci puis ont su extraire sa définition après avoir analysé algébriquement le rectangle d'or.

Nous pouvons voir ci-dessus quelques productions d'élèves de la spirale d'or proposées lors de l'exposition mentionnée précédemment.

![](images/EPI_nbre_dor_03.jpg){ width=50%; : .center }

![](images/EPI_nbre_dor_04.jpg){ width=50%; : .center }



