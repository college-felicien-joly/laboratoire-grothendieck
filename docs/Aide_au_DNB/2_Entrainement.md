---
author: Anthony Salze
title: Entraînement et révisions
hide:
    -toc
    -integrate
    -footer
---
## <span style=color:Tomato> Puissances
[Exercices](https://coopmaths.fr/alea/?uuid=eb865&id=3C10-1&i=1&uuid=379cd&id=3C10-2&i=1&uuid=31bd7&id=3C10-3&i=1&v=eleve&es=0111011&title=Puissances){:target="_blank"}  

## <span style=color:Tomato> Pythagore
[Exercices](https://coopmaths.fr/alea/?uuid=bd660&id=4G20&uuid=40c47&id=4G20-1&uuid=ab5d4&id=4G21&uuid=b18e8&id=4G22&v=eleve&es=1210011&title=Pythagore){:target="_blank"}  

## <span style=color:Tomato> Calcul littéral
[Simplifier une expression littérale et réduire](https://coopmaths.fr/alea/?uuid=603a8&id=3L10&i=1&uuid=815eb&id=3L10-1&uuid=c88ba&id=3L10-2&v=eleve&es=1110011&title=Calcul+litt%C3%A9ral+1){:target="_blank"}  

[Développer et réduire (simple, double distributivités et identités remarquables)](https://coopmaths.fr/alea/?uuid=db2e0&id=3L11&i=1&uuid=4197c&id=3L11-1&i=1&uuid=82313&id=3L11-3&i=1&uuid=7cf81&id=3L11-7&i=1&uuid=be157&id=3L12-1&i=1&v=eleve&es=1110011&title=Calcul+litt%C3%A9ral+1){:target="_blank"}  

[Factorisation](https://coopmaths.fr/alea/?uuid=5f5a6&id=3L11-4&i=1&uuid=51360&id=3L11-6&i=1&v=eleve&es=1110011&title=Calcul+litt%C3%A9ral+31){:target="_blank"}  

[Résoudre une équation](https://coopmaths.fr/alea/?uuid=5a02b&id=3L13-0&i=1&uuid=f239f&id=3L13&i=1&uuid=1802d&id=3L13-1&i=1&uuid=22412&id=3L13-3&i=1&uuid=cd2f2&id=3L13-4&i=1&v=eleve&es=1110011&title=Calcul+litt%C3%A9ral+4){:target="_blank"}  