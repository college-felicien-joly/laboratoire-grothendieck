---
author: Anthony Salze & Jean-Sébastien Sip
title: Aide au DNB
hide:
    -toc
    -integrate
    -footer
---

## <span style=color:Tomato> Horaires de l'Aide aux devoirs en Mathématiques

* **M. SIP** : le jeudi de 12h40 à 13h20 en A218
* **Mme LECAT** : le lundi et le jeudi de 12h40 à 13h20 en A109
* **Mme ANCIAUX** : le mardi de 12h40 à 13h20 en A126