---
author: Anthony Salze & Jean-Sébastien Sip
title: Liens divers
hide:
    -navigation
    -toc
    -integrate
    -footer
---
??? info "Site internet membre du laboratoire"
    - Site leçon, exercices cycle 4 : [*Anthony Salze*](https://anthony_salze.forge.apps.education.fr/officiel/){:target="_blank"}

??? info "Concours mathématiques"
    - [Le Kangourou des Mathématiques](https://www.mathkang.org/concours/){:target="_blank"}
    - [Castor Informatique](https://castor-informatique.fr/){:target="_blank"}
    - [Algorea](https://algorea.org/#/){:target="_blank"}
    - [Course aux nombres](https://codimd.apps.education.fr/s/7G91CuKJR#){:target="_blank"}
    - [Pangea](https://www.concourspangea.org/historique/#){:target="_blank"}


??? info "Exerciseurs, Annales"
    - Calcul mental : [*MathsMentales*](https://mathsmentales.net/){:target="_blank"}
    - Exerciseurs : [*CoopMaths*](https://coopmaths.fr/alea/){:target="_blank"}
    - Annales examens et concours : [*APMEP*](https://www.apmep.fr/Annales-examens-Brevet-CAP-BEP-Bac-BTS-et-concours-niveau-Terminale){:target="_blank"}
    - Ressources diverses : [*Bibm@th*](https://www.bibmath.net/){:target="_blank"}

??? info "Recherche"
    - **I**nstitut de **R**echerche sur l'**E**nseignement des **M**athématiques : [*IREM*](https://www.univ-irem.fr/index.php){:target="_blank"}
    - **IREM** : [*Lille*](https://irem.univ-lille.fr/){:target="_blank"}

??? info "Histoire des mathématiques"
    - Ressources diverses : [*Histoires de Mathématiques*](https://www.hist-math.fr/notice.html){:target="_blank"}
    - Jeu vidéo : [*Mathêma*](https://anthony_salze.forge.apps.education.fr/mathema/){:target="_blank"}

??? info "Associations mathématiques et informatiques"
    - **A**ssociation des **P**rofesseurs de **M**athématiques de l'**E**nseignement **P**ublic : [*APMEP*](https://www.apmep.fr/){:target="_blank"}
    - **APMEP** : Régionale de Lille : [*APMEP Lille*](https://apmeplille.fr/){:target="_blank"}
    - **S**ociété **M**athématique de **F**rance : [*SMF*](https://smf.emath.fr/){:target="_blank"}
    - **A**ssociation des **E**nseignantes et **E**nseignants d'**I**nformatique de **F**rance : [*AEIF*](https://aeif.fr/){:target="_blank"}

??? info "Revues, magazines"
    - Publimaths : [*Publimath*](https://publimath.univ-irem.fr/){:target="_blank"}
    - Tangente Magazine : [*Tangente*](https://www.tangente-mag.com/){:target="_blank"}
    - Quadrature : magazine de mathématiques pures et épicées : [*Quadrature*](https://www.quadrature-mag.fr/){:target="_blank"}