---
author: Anthony Salze & Jean-Sébastien Sip
title: Accueil
hide:
    -toc
    -integrate
    -footer
---
Le langage ***Python*** est un langage informatique créé par Guido van Rossum dans les années 1980. Le nom provient de la célèbre émission _Monty Python's Flying Circus_, série télévisée britannique dont il est fan.
	
Python est un langage interprété fortement typé de haut niveau et modulaire. De fait, il s’agit d’un langage particulièrement adapté pour débuter en informatique dans la rédaction des algorithmes. En effet, nous voyons un très grand nombre d’avantages dans l’utilisation de ce langage dans nos classes comme par exemple la nécessité de taper précisément le texte algorithmique, l’utilisation de l’anglais à travers des mots souvent transparents, le développement de l’esprit critique et de l’auto-correction.
	
Mais bien plus qu’un simple langage algorithmique, Python peut être d’une aide considérable dans l’acquisition de notions enseignées au collège, comme par exemple en calcul littéral : rôle de la lettre en Mathématiques (nom, représentation d’un nombre indéterminé, variable) ainsi que du rôle du signe « égal » à travers notamment l’utilisation du module « sympy ».
	
Comme nous le verrons dans cette section, de nombreuses activités exploitent les différents modules développés afin d’aider et accompagner les élèves dans l’acquisition des savoirs mathématiques ainsi qu’informatiques. En effet, la montée en puissance de l’utilisation des « Intelligences Artificielles » doit nous préoccuper : il est fort probable que nos élèves soient amenés à utiliser de plus en plus l’IA dans leur métier futur. Cela nécessite l’acquisition de compétences afin de développer une utilisation raisonnée et responsable de cet outil tout en gardant un œil avisé et critique vis-à-vis des résultats produits. Ainsi, l’utilisation conjointe de Python et de leurs travaux en classe et/ou à la maison nous semble être un atout majeur afin de les préparer à ce futur. 