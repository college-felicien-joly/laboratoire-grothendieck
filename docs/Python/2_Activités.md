---
author: Anthony Salze & Jean-Sébastien Sip
title: Activités
hide:
    -toc
    -integrate
    -footer
---
Activités en lien avec Python