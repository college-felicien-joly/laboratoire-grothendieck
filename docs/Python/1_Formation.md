---
author: Anthony Salze & Jean-Sébastien Sip
title: Formation
hide:
    -toc
    -integrate
    -footer
---
Formation Python animée par Jean-Sébastien SIP sur l'utilisation de Python au collège à destination des enseignants.
