---
author: Anthony Salze & Jean-Sébastien Sip
title: Énigme du mois
hide:
    -toc
    -integrate
    -footer
---
## Octobre 2024
- 1) 4$^{\text{ème}}$ / 3$^{\text{ème}}$  
Nous disposons de 6 allumettes ayant toutes la même longueur.  
**Construire exactement 4 triangles équilatéraux identiques avec ces allumettes sans les casser.**


- 2) 6$^{\text{ème}}$ / 5$^{\text{ème}}$  
Nous disposons de 6 allumettes ayant toutes la même longueur.  
**Construire exactement 6 triangles équilatéraux identiques avec ces allumettes sans les casser.**
