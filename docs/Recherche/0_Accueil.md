---
author: Anthony Salze & Jean-Sébastien Sip
title: Accueil
hide:
    -toc
    -integrate
    -footer
---
Dans son œuvre, Alexandre Grothendieck s’est illustré tout particulièrement à travers la façon de concevoir les concepts mathématiques et les relations que ces derniers entretiennent les uns envers les autres.

C'est dans cette dynamique que se cristallisent les travaux de recherche didactique que mène ce laboratoire. La ***vision***, terme fondamental chez Grothendieck, à travers laquelle s’incarne tout concept mathématique dans sa manipulation, dans son interaction avec son environnement et dans son abstraction est au cœur de chaque réflexion menée avec nos élèves.

Pour ce faire, la Théorie des Catégories constitue l’un des piliers nécessaires à l’élaboration d’un langage pertinent dans notre approche didactique. Grothendieck lui-même fut l’un des principaux instigateurs de cette pensée en France. Cette approche originale s’articule autour de la philosophie synthétique des mathématiques qui se concentre essentiellement sur les connexions entre objets que la pensée mathématique révèle dans son action. En cela, elle se distingue de la philosophie analytique qui s’attache à définir les objets mathématiques à travers les éléments qui les constituent, apanage de la Théorie des Ensembles qui régule les Mathématiques depuis le siècle dernier et qui a été initiée par l’école Bourbaki.

Mais il est indispensable pour nous de souligner qu’il ne s’agit en aucun cas de négliger l’une des philosophies par rapport à l’autre. Bien au contraire, nous sommes totalement convaincus que c’est à travers une symbiose réfléchie des fruits de ces réflexions analytiques et synthétiques que nous pourrons entrevoir une ***vision*** didactique nouvelle dont pourront bénéficier nos élèves par le biais d’activités inédites.

Cela dit, la vision synthétique offerte par la Théorie des Catégories (ainsi que par celle des Topos) est peu traditionnelle dans les cursus universitaires ordinaires que les enseignants ont pu suivre. C’est la raison pour laquelle un volet important dans nos recherches s’enracine dans cette Théorie dont nous proposons un aperçu solide à travers des formations.
