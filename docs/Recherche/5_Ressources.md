---
author: Anthony Salze & Jean-Sébastien Sip
title: Ressources
hide:
    -toc
    -integrate
    -footer
---
## Conférences

* Conférence donnée le 22/01/2025 par Bertrand Toën dans le cadre du cycle, << Un texte, une aventure mathématique >> organisée par la SMF et la BNF. 
[Les mille et une pages mathématiques d'Alexandre Grothendieck](https://www.youtube.com/watch?v=xl9JF1AOCRw){:target="_blank"}

* Autres conférences

## Podcasts

* <strong>Alexandre Grothendieck, légende rebelle des mathématiques</strong>
Alexandre Grothendieck est l’un des plus grands mathématiciens du 20ᵉ siècle, l’un des plus oubliés aussi. Tour à tour chercheur à la renommée internationale, pionnier de l’écologie, écrivain, mystique, philosophe, ermite… Que nous racontent les vies successives de ce génie marginal ? En 5 épisodes.
[Alexandre Grothendieck, légende rebelle des mathématiques](https://www.radiofrance.fr/franceculture/podcasts/serie-alexandre-grothendieck-legende-rebelle-des-mathematiques){:target="_blank"}

* <strong>Grothendieck : la moisson</strong>
Comment appréhender cette œuvre complexe qu'est <strong>Récoltes et Semailles</strong> mêlant mathématiques, autobiographie, réflexions philosophiques et poétiques ? Discussion avec les mathématiciens Olivia Caramello, Alain Connes et Laurent Lafforgue.
[Grothendieck : la moisson](https://www.radiofrance.fr/franceculture/podcasts/la-methode-scientifique/grothendieck-la-moisson-5643669){:target="_blank"}
