---
author: Anthony Salze & Jean-Sébastien Sip
title: Accueil
hide:
    -toc
    -integrate
    -footer
    -navigation
---

![Logo](images/Logo_AG_FJ.png)

# Bienvenue !

Le Laboratoire Alexandre Grothendieck est un Laboratoire de Mathématiques qui s'inscrit dans la mission engagée par C. Villani et C. Torossian dans le but de promouvoir un lieu de formation et de développement professionnel au sein du collège Félicien Joly de Fresnes sur Escaut (59). Il s'agit également d'un lieu d’expérimentation et d’innovation orienté vers la production de ressources originales et inédites. Ce Laboratoire offre également une ouverture sur son territoire permettant des échanges dynamiques entre différents acteurs. Mais surtout, le Laboratoire Alexandre Grothendieck propose un axe recherche (pédagogique, didactique, universitaire) ouvert sur les autres sciences mais également sur les arts (musique, arts plastiques...), la linguistique, la littérature, la philosophie, etc. 

Le Laboratoire proposera régulièrement différentes activités réalisées au collège par l'équipe  de Mathématiques mettant en œuvre la connexion que cette matière entretient avec les différentes disciplines de l’établissement.

## Python
![Logo Python](images/Python-Logo.png){ width=50%; : .center }
Une attention toute particulière se portera sur l’utilisation du langage informatique Python. De nombreuses activités (de la cinquième à la troisième) seront proposées afin de mettre en avant non seulement la puissance de son langage en algorithmique mais également son avantage pédagogique certain dans l’acquisition des nombreuses notions enseignées au collège. Des formations seront également proposées.

## LaTeX
![Logo Python](images/LaTeX_logo.png){ width=50%; : .center }
L'utilisation de LaTeX nous semble être une solution commode et idéale dans la conception de différentes activités au collège. De plus, l'utilisation de la classe _beamer_ permet de réaliser des cours projetés agréables à lire pour les élèves. Le Laboratoire proposera donc de nombreuses productions afin de généraliser son utilisation en collège.

## Catégories
![Categories pushout](images/categories_pushout.png){ : .center  }
L’axe fort de ce Laboratoire consiste en son volet recherche didactique et pédagogique s’articulant essentiellement sur une vision originale de l’enseignement des Mathématiques dans l’esprit de la philosophie synthétique dont Alexandre Grothendieck fut l’un des précurseurs en France. Un travail régulier sera fourni proposant en particulier des formations mais également des activités et des propositions de rencontre interlaboratoire afin de nourrir la réflexion engagée par l’équipe du Laboratoire. Par exemple, un cours solide sur la Théorie des Catégories pourra être assuré.

Enfin, bien d’autres choses viendront alimenter le site comme des énigmes, des aides de travail, cartes mentales, etc.


## Remerciements
Le site est hébergé par la forge de [l'*Association des Enseignantes et Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/){:target="_blank"}.

Nous remerçions chaleureusement Hugues Créteur pour la réalisation du logo du laboratoire.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){:target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){:target="_blank"}.

<span style=color:LightSalmon>_Dernière mise à jour le 11/02/2025_
